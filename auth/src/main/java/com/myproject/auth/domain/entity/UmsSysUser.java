package com.myproject.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@TableName("ums_sys_user")
public class UmsSysUser implements Serializable {
    /*
     * 主键
     */
    @TableId
    private Integer id;
    private String username;
    private String nickname;
    private String email;
    private String mobile;
    private Integer sex;
    private String avatar;
    private String password;
    private Integer status;
    private String creator;
    private LocalDate createTime;
    private String updater;
    private LocalDate updateTime;
    private String remark;
    /*
    * 逻辑删除
    * 0:删除
    * 1:未删除
     */
    @TableLogic(value = "1", delval = "0")
    private Integer deleted;

}
