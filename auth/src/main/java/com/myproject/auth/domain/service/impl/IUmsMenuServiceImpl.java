package com.myproject.auth.domain.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.myproject.auth.domain.entity.UmsMenu;
import com.myproject.auth.domain.mapper.UmsMenuMapper;
import com.myproject.auth.domain.service.IUmsMenuService;
import org.springframework.stereotype.Service;

@Service
public class IUmsMenuServiceImpl extends ServiceImpl<UmsMenuMapper, UmsMenu> implements IUmsMenuService {
}
