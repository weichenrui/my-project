package com.myproject.auth.domain.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.myproject.auth.domain.entity.UmsRole;
import com.myproject.auth.domain.mapper.UmsRoleMapper;
import com.myproject.auth.domain.service.IUmsRoleService;
import org.springframework.stereotype.Service;

@Service
public class IUmsRoleServiceImpl extends ServiceImpl<UmsRoleMapper, UmsRole> implements IUmsRoleService {
}
