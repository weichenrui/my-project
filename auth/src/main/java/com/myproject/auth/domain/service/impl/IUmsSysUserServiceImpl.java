package com.myproject.auth.domain.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.myproject.auth.domain.entity.UmsSysUser;
import com.myproject.auth.domain.mapper.UmsSysUserMapper;
import com.myproject.auth.domain.service.IUmsSysUserService;
import org.springframework.stereotype.Service;

@Service
public class IUmsSysUserServiceImpl extends ServiceImpl<UmsSysUserMapper, UmsSysUser> implements IUmsSysUserService{

}
