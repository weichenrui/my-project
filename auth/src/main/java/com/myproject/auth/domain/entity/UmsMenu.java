package com.myproject.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@TableName("ums_menu")
public class UmsMenu implements Serializable {
    @TableId
    private Long id;
    private Long parentId;
    private String menuName;
    private Integer sort;
    private Integer menuType;
    private String path;
    private String componentPath;
    private String perms;
    private String icon;
    private Integer deleted;
    private Integer status;
    private String creator;
    private String updater;
    private LocalDate create_time;
    private LocalDate update_time;

}
