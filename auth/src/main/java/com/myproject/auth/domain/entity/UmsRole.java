package com.myproject.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@TableName("ums_role")
public class UmsRole implements Serializable {
    @TableId("role_id")
    private Integer roleId;
    private String roleName;
    private String roleLabel;
    private Integer sort;
    private Integer status;
    private Integer deleted;
    private String remark;
    private String creator;
    private String updater;
    private LocalDate create_time;
    private LocalDate update_time;

}
