package com.myproject.auth.domain.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.myproject.auth.domain.entity.UmsSysUser;

public interface IUmsSysUserService extends IService<UmsSysUser> {
}
